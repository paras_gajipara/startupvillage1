<!DOCTYPE html>
<html lang="en">
<?php
error_reporting(0);
require_once 'includes/functions.php';
session_start();
?>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>StartUp</title>

	<!-- Global stylesheets -->
	<link href="css/css.css" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript">
		$('#searchid').keypress(function (e) {
		 var key = e.which;
		 if(key == 13)  // the enter key code
		  {
		    $('#search-btn').click();
		    return false;  
		  }
		});  

		$(function(){
		$("#search-btn").click(function() 
		{ 
		var searchid = $(".search").val();
		var dataString = 'search='+ searchid;
		if(searchid!='')
		{
		    $.ajax({
		    type: "POST",
		    url: "search.php",
		    beforeSend: function(){
		                       $("#loaderDiv").show();
		                       $("#result").hide();
		                   },
		    data: dataString,
		    cache: true,
		    success: function(html)
		    {
		    $(".loader").hide();
		    $("#result").html(html).show();
		    
		    }
		    });
		}
		else
		{
		  $("#result").hide();
		}return false;    
		});


		$("a#list").click(function() 
		{ 
		var searchid = $(this).text();
		var dataString = 'search='+ searchid;
		if(searchid!='')
		{
		    $.ajax({
		    type: "POST",
		    url: "search.php",
		    beforeSend: function(){
		                       $("#loaderDiv").show();
		                       $("#result").hide();
		                   },
		    data: dataString,
		    cache: false,
		    success: function(html)
		    {
		    $(".loader").hide();
		    $("#result").html(html).show();
		    
		    }
		    });
		}
		else
		{
		  $("#result").hide();
		}return false;    
		});

		jQuery("#result").live("click",function(e){ 
		    var $clicked = $(e.target);
		    var $name = $clicked.find('.name').html();
		    var decoded = $("<div/>").html($name).text();
		    $('#searchid').val(decoded);
		});
		jQuery(document).live("click", function(e) { 
		    var $clicked = $(e.target);
		    if (! $clicked.hasClass("search")){
		    jQuery("#result").fadeOut(); 
		    }
		});
		$('#searchid').click(function(){
		    jQuery("#result").fadeIn();
		});

		});
	</script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>

	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<script type="text/javascript" src="assets/js/pages/dashboard_boxed.js"></script>
	<!-- /theme JS files -->

</head>

<body class="layout-boxed sidebar-xs">

	<!-- Main navbar -->
	<div class="navbar navbar-inverse bg-indigo-400">
		<div class="navbar-header">
			<a class="navbar-brand" href="index.php"><img src="assets/images/logo_l.png" height="38px" alt=""></a>
			<!-- <a class="navbar-brand" href="about.html">About Us</a>
 -->
			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			
			<div class="col-sm-3 col-md-3 pull-right">
		        <!-- <form class="navbar-form" method="post" role="search"> -->
		        <div class="input-group">
		            <input type="text"  class="form-control search" placeholder="search" name="search" id="searchid">
		            <div class="input-group-btn">
		                <button class="btn btn-default" id="search-btn"><i class="glyphicon glyphicon-search"></i></button>
		            </div>
		        </div>
		       <!--  </form> -->
		    </div>

		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">
									
								<!-- Main -->
								<li class="navigation-header"><span>Categories</span> <i class="icon-menu" title="Main pages"></i></li>
								<li>
									<a href="#"><i class="glyphicon glyphicon-phone"></i> <span>Electronics</span></a>
									<ul>
										<li>
											<a id="list" onclick="list()">Mobiles</a>
											<ul>
												<li><a id="list" onclick="list()">Motorola</a></li>
												<li><a id="list" onclick="list()">Lenovo</a></li>
												<li><a id="list" onclick="list()">Mi</a></li>
											</ul>
										</li>
										<li>
											<a href="#">Laptops</a>
											<ul>
												<li><a id="list" onclick="list()">HP</a></li>
												<li><a id="list" onclick="list()">Lenovo</a></li>
												<li><a id="list" onclick="list()">Dell</a></li>
											</ul>
										</li>
									</ul>
								</li>
								<li>
									<a href="#"><i class="glyphicon glyphicon glyphicon-blackboard"></i> <span>Air Conditioners</span></a>
									<ul>
										<li>
											<a id="list" onclick="list()">Air Conditioners</a>
											
										</li>
									</ul>
								</li>
								<li>
									<a href="#"><i class="glyphicon glyphicon-picture"></i> <span>Televisions</span></a>
									<ul>
										<li>
											<a id="list" onclick="list()">Televisions</a>
											<ul>
												<li><a id="list" onclick="list()">LG television</a></li>
												<li><a id="list" onclick="list()">Samsung television</a></li>
												<li><a id="list" onclick="list()">videocon television</a></li>
												<li><a id="list" onclick="list()">Sony television</a></li>
											</ul>
										</li>
									</ul>
								</li>
								
								
								
								<!-- /main -->

								

							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							
						</div>
						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="index.php" class="btn btn-link btn-float has-text"><i class="icon-home2 text-primary"></i><span>Home</span></a>
								<a href="about.html" class="btn btn-link btn-float has-text"><i class="icon-notebook text-primary"></i><span>About Us</span></a>
								
							</div>
						</div>
					</div>

					
				</div>
				<!-- /page header -->