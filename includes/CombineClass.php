<?php 
	
	$index = 0;
	class Product {
		protected $product_Price;
		protected $product_MinPrice;
		protected $product_Id;
		protected $product_Title;
		protected $image_Url;
		protected $product_Url;
		protected $product_Description;
		protected $product_Brand;
		protected $product_DetailUrl;
		protected $product_Image;
		

		function __construct($product_Title , $product_MinPrice,  $image_Url , $product_DetailUrl) {
			global $index;
			$this->product_Title = $product_Title;
			$this->product_MinPrice = $product_MinPrice;
			$this->image_Url = $image_Url;
			$this->product_DetailUrl = $product_DetailUrl;
			$this->product_Id = $index++;
		}


		function getproductPrice(){return $this->product_Price;}
		function getMinPrice(){return $this->product_MinPrice;}
		function getproductId(){return $this->product_Id;}
		function getproductTitle(){return $this->product_Title;}
		function getImageUrl(){return $this->image_Url;}
		function getproductUrl(){return $this->product_Url;}
		function getproductDescription(){return $this->product_Description;}
		function getproductBrand(){return $this->product_Brand;}
		function getproductDetailUrl(){ return $this->product_DetailUrl;}
		function getPrice(){return $this->product_Price;}
		function getImage(){return $this->product_Image;}
		function setPrice($price){$this->product_Price = $price;}
		function setImage($image){$this->product_Image = $image;}
		function setUrl($url){$this->product_Url = $url;}
		function getUrl(){return $this->product_Url;}
		function setDescription($feature){ 
			$x = '<ul>'.$feature.'</ul>';
			$this->getproductDescription = $x;
		 }
	}
	
?>