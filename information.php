<?php
require_once 'header.php';
require_once 'includes/functions.php';
$products = unserialize($_SESSION['products']);
$pid=$_GET['pid'];

$product = searchById($pid , $products);

$productId = $product->getProductId();
$title = $product->getProductTitle();
$productImage = $product->getImageUrl();
$productDescription = $product->getproductDescription;
$image = $product->getImage();
$price = $product->getPrice();
$url = $product->getUrl();
for($i = 1 ; $i <= count($url) ; $i++ ){

    if(strpos($url[$i], "flipkart") != false) {
        $tempNumber = strpos($url[$i], "affid");
        $tempUrl = substr($url[$i] , 0 , $tempNumber+6 );
        $url[$i] = $tempUrl.'parasgaji';   
    }
    else if (strpos($url[$i], "amazon") != false) {
        $tempNumber = strpos($url[$i], "tag");
        $tempUrl = substr($url[$i] , 0 , $tempNumber+4 );
        $url[$i] = $tempUrl.'unity0f-21';
    }
    else {

    }
}
?>    <div class="loader" style="display:none;" id="loaderDiv">Loading...</div>
                <!-- Content area -->
 <div id="result">
                <div class="content" >

                    <!-- Square thumbs -->
                    <h4 class="content-group text-semibold">
                        <?php echo $title;?>
                    </h4>
                   <div style="float:left; width:300px;height:300px;margin-bottom:5px">
                    <hr>
                    <img class="img-responsive" src="<?php echo $productImage; ?>" alt="">
                    <br></div>
                    <div class="col-md-6">
                    
                    <div class="row brands">
                    <br><br>
                    <?php 
                    for($i=1;$i <= count($url) ; $i++ ){ ?>
                    <div class='col-md-4' style="border-right:1px solid grey;padding-left:10px;">
                    <img src="<?php echo $image[$i]; ?>" ><br/><br>
                    <b><?php echo $price[$i]; ?></b><br/><br>
                    <a href="<?php echo $url[$i]; ?>" target="_blank" class="btn btn-success btn-raised hvr-icon-forward">Buy Now&nbsp;&nbsp;</a>
                    </div><span>   </span>

    <?php } 
    ?>

                    </div>
                    </div>
                    <!-- Simple panel -->
                    </div>
                    <div class="panel panel-flat" >
                        <div class="panel-heading">
                            <u><h5 class="panel-title"><b>Product Description</b></h5></u>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                    <li><a data-action="close"></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="panel-body">
                            <b style="font-size: medium;"><?php
                            if(isset($productDescription)) 
                                echo $productDescription;
                            else
                                echo"Product Description is not available";
                            ?>
                            </b>
                      </div>
                    </div>
                    <!-- /simple panel -->

</div>             
<?php
include("footer.php");
?>