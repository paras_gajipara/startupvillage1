<?php 
session_start();
require_once 'includes/functions.php';

$keyword = "nokia";
if(isset($_POST['search']))
{
$keyword=$_POST['search'];
$products=getProductByKeyword($keyword);
}
else{
$products=getProductByKeyword($keyword);
}
?>
		
				<!-- Content area -->
			<div class="loader" style="display:none;" id="loaderDiv">Loading...</div>
				<div class="content" id="result">
						
					<!-- Square thumbs -->
					<h4 class="content-group text-semibold">
						<span class="icon-star-full2"></span> | Featured
					</h4>

					<hr>
					<?php
							$count=0;
							$i=0;
							foreach ($products as $product) {
								$title=$product->getproductTitle();
								$productImage = $product->getImageUrl();
								$sellingPrice=$product->getMinPrice();
								$currency="Rs.";
								$productUrl = $product->getproductUrl();
								$productId = $product->getproductId();
							?>

							<?php if($count%4==0)
									echo '<div class="row">';
							?>
							<div class="col-lg-3 col-md-6">
								<div class="thumbnail no-padding">
									<div class="thumb" >
										<img src="<?php echo $productImage;?>" style="max-width:200px; max-height:200px; text-align:center;">
										<div class="caption-overflow">
										<span>
											<a href="information.php?pid=<?php echo $productId;?>" class="btn bg-success-400 btn-icon btn-xs" data-popup="lightbox"><i class="icon-plus2"></i></a>
										</span>
										</div>
									</div>
									
									<div class="caption text-center">
									<h4><a href="information.php?pid=<?php echo $productId;?>"><?php echo $title;?></a></h4>
									Selling Price: <b><?php echo $currency."&nbsp;".$sellingPrice;?></b></div>
								</div>
							</div>
					<?php 
							if(($i+1)%4==0)
								echo '</div>';
					?>
					<?php
							$i++;
							$count++;
							}

					?>
					
				</div>
				
			</div>
			<!-- main container -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>

